#! awk -f
# celesc.awk - gera um report baseado num pdf

function getUC() {
    #procura a UC
    where = match($0, /(\r?\n)+UC:(\r?\n)+([[:digit:]]+)\r?\n/, extract) #the 3rd argument is a gnu extension. non portable
    #existem 2 possiveis posições onde esta informação pode estar.
    if (where) {
        #print "encontrou UC em " where
        #imprime a UC
        return extract[3]
    }
}

function getIdentificador() {
    return "id"
}

function getMesReferencia() {
    where = match($0, /Referência: ([[:digit:]]{2}\-[[:digit:]]{4})/, extract)
    #where = match($0, /Referência:(\r?\n)+(.+)(\r?\n)+(.+)(\r?\n){2}/, extract)
    if (where) {
        return extract[1]
        #printf "encontrou referencia em %s", where
        #print extract[2]
    #    print "length(extract[2]) = " length(extract[2])
    }
}

function getValor() {
    return "R$ 0,00"
}

function getConsumo() {
    return "0 kwh"
}

BEGIN {
	if (ARGC < 2) {
		print "usage: celesc.awk input"
		exit(1)
	}

	if (ARGV[1] ~ /.+\.pdf/) {
		print "pdftotext " ARGV[1] " " ARGV[1] ".txt"
		#system("pdftotext arquivo_pdf arquivo_txt")
        # adiciona o arquivo novo na fila e avança para o proximo arquivo
        # ver no manual, na seção de programas uteis.
        # Existe um exemplo que mostra como fazer isso.
		exit(0)
	}

	RS="\f"
	FS="(\r?\n)+"

	print "UC; Identificador; Mes-referência; Valor; Consumo"
}

{
    NumRegistros++
    printf "%s; %s; %s; %s; %s\n", getUC(), getIdentificador(), getMesReferencia(), getValor(), getConsumo()
}

END {
    print "NR = " NR > "/dev/stderr"
	#PROCINFO["sorted_in"] = 1
	if (NumRegistrosErrados) {
		print "NumRegistrosErrados=" NumRegistrosErrados > "/dev/stderr"
	}
}
