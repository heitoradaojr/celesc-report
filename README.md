This  Awk script sumarizes a report from a local concessionary of energy, given in PDF format, and creates a spreadsheet (CSV).

A saída tem que ter 4 colunas.
1) UC - Unidade Consumidora
2) Nome do lugar (verifinar no endereço)
3) consumo (kwh)
4) data de vencimento ou mês de referência
5) custo da fatura
6) corte

O programa deve estar protegido contra tentativa de processamento do mesmo arquivo várias vezes.
